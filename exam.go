package main

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"github.com/gorilla/mux"
	
	_ "github.com/mattn/go-sqlite3"
)

const db string = ""

type question struct {
	Id string `json:"id"`
	Answer string `json:"answer"`
	Question string `json:"question"`
	Opt1 string `json:"opt1"`
	Opt2 string `json:"opt2"`
	Opt3 string `json:"opt3"`
	Opt4 string `json:"opt4"`
	Opt5 string `json:"opt5"`
	Opt6 string `json:"opt6"`
	Opt7 string `json:"opt7"`
	Opt8 string `json:"opt8"`
}

func getQuestion(w http.ResponseWriter, r *http.Request) {
	var q question
	
	vars := mux.Vars(r)
    qid := vars["qid"]
	
	database, _ := sql.Open("sqlite3", db)
	defer database.Close()
	rows, _ := database.Query("select * from exam where id = ?", qid)
	defer rows.Close()
		
	for rows.Next() {
        rows.Scan(&q.Id, &q.Answer, &q.Question, &q.Opt1, &q.Opt2, &q.Opt3, &q.Opt4, &q.Opt5, &q.Opt6, &q.Opt7, &q.Opt8)
    }

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(http.StatusOK)
    if err := json.NewEncoder(w).Encode(q); err != nil {
        panic(err)
    }
}

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/{qid}", getQuestion)
	http.ListenAndServe(":8090", router)
}
