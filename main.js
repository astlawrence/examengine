function ans() {
	var a = document.getElementById("ans");
	a.style.display = "block";
}

function restart() {
	sessionStorage.clear();
}

function next() {
	const size = 100;
	var idsArray;
	var r;
		
	document.getElementById('aR').style.display = "none";
	document.getElementById('bR').style.display = "none";
	document.getElementById('cR').style.display = "none";
	document.getElementById('dR').style.display = "none";
	document.getElementById('eR').style.display = "none";
	document.getElementById('fR').style.display = "none";
	document.getElementById('gR').style.display = "none";
	document.getElementById('hR').style.display = "none";
		
	document.getElementById('ans').style.display = "none";
 
	r = Math.floor(Math.random() * size) + 1;
 
	if (sessionStorage.getItem('ids')) {
		idsArray = JSON.parse(sessionStorage.getItem('ids'));
	} else {
		idsArray = [];
	}
       
    if (idsArray.length < (size + 1)) {
		while (idsArray.includes(r)) {
			r = Math.floor(Math.random() * size) + 1;
		}
	} else {
		document.getElementById('question').innerHTML = "End of exam.";
		return;
	}
    idsArray.push(r);
    sessionStorage.setItem('ids', JSON.stringify(idsArray));
      
	var url = "http://127.0.0.1:8090/" + r
		
	axios({ method: "GET", url: url, headers: {"content-type": "text/plain" } }).then(result => { 
		document.getElementById('question').innerHTML = result.data['question'];
			
		if (result.data['opt1']) {
			document.getElementById('a').innerHTML = result.data['opt1'];
			document.getElementById('aC').checked = false;
			document.getElementById('aR').style.display = "block";
		}
		if (result.data['opt2']) {
			document.getElementById('b').innerHTML = result.data['opt2'];
			document.getElementById('bC').checked = false;
			document.getElementById('bR').style.display = "block";
		}
		if (result.data['opt3']) {
			document.getElementById('c').innerHTML = result.data['opt3'];
			document.getElementById('cC').checked = false;
			document.getElementById('cR').style.display = "block";
		}
		if (result.data['opt4']) {
			document.getElementById('d').innerHTML = result.data['opt4'];
			document.getElementById('dC').checked = false;
			document.getElementById('dR').style.display = "block";
		}
		if (result.data['opt5']) {
			document.getElementById('e').innerHTML = result.data['opt5'];
			document.getElementById('eC').checked = false;
			document.getElementById('eR').style.display = "block";
		}
		if (result.data['opt6']) {
			document.getElementById('f').innerHTML = result.data['opt6'];
			document.getElementById('fC').checked = false;
			document.getElementById('fR').style.display = "block";
		}
		if (result.data['opt7']) {
			document.getElementById('g').innerHTML = result.data['opt7'];
			document.getElementById('gC').checked = false;
			document.getElementById('gR').style.display = "block";
		}
		if (result.data['opt8']) {
			document.getElementById('h').innerHTML = result.data['opt8'];
			document.getElementById('hC').checked = false;
			document.getElementById('hR').style.display = "block";
		}
		
		document.getElementById('ans').innerHTML = result.data['answer'];
	})
}

